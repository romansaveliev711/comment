using UnityEngine;
using System;
using System.Collections;

public class Cooldown: MonoBehaviour
{
	public event EventHandler<TimeToSpownEventArgs> OnTimetoSpown;
	[SerializeField]private float cooldownTime;
	private float timeStart;
	private float timeLeft;

	void Start () {
		timeStart = Time.time;  //������������ ���������� �� �������(���������� ��������� ���� ��� !!!! ��� ���� ��� �� ��������� ����)
	}


	void Update () {
		timeLeft = Time.time - timeStart;  // ������ ������� �� ������������� ������� �� ������
		if(cooldownTime<= timeLeft)    
		{
			timeStart = Time.time;    

			if (OnTimetoSpown != null)      // ���� ����� ������ ������ ������������...(� �� ��� ���������� �� ��������� ������� ��� � ����� ���� �������)
			{
				OnTimetoSpown (this, new TimeToSpownEventArgs (this));//...�� �� �������� ������� ������
			}
		}
	}
	public class TimeToSpownEventArgs : EventArgs        //�� ��� ��� �������,�� ������������ ��������� ������ ������� ������ ����
	{
		public Cooldown SpownBubble { get; private set; }
		public TimeToSpownEventArgs(Cooldown spownObject)
		{
			SpownBubble = spownObject;
		}
	}
}